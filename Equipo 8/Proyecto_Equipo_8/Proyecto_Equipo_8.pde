import peasy.*;

PeasyCam cam; //requiere la importación de la librería
ArrayList<Planeta> sistema = new ArrayList<Planeta>();
PShape sol;

int speed = 1; // multiplicador de velocidad

void setup() {
  fullScreen(P3D); //define el tamaño de la pantalla y el espacio tridimensional
  noStroke();
  cam = new PeasyCam(this, 1200); //Se declara la cámara de la librería peasycam

  //Creamos todos los planetas y el sol
  sol = createShape(SPHERE, 100);
  sol.setTexture(loadImage("sun.jpg")); // sol
  sistema.add(new Planeta(200, 10, 0.00172*speed, loadImage("mercury.jpg"), 1)); // mercurio
  sistema.add(new Planeta(250, 10, 0.00126*speed, loadImage("venus.jpg"), 2)); // venus
  sistema.add(new Planeta(300, 15, 0.00107*speed, loadImage("earth.jpg"), 3)); // tierra
  sistema.add(new Planeta(350, 14, 0.00086*speed, loadImage("mars.jpg"), 4)); // marte
  sistema.add(new Planeta(600, 60, 0.00047*speed, loadImage("jupiter.jpg"), 5)); // jupiter
  sistema.add(new Planeta(800, 50, 0.00034*speed, loadImage("saturn.jpg"), 6)); // saturno
  sistema.add(new Planeta(950, 30, 0.00024*speed, loadImage("uranus.jpg"), 7)); // urano
  sistema.add(new Planeta(1050, 30, 0.00019*speed, loadImage("neptune.jpg"), 8)); // neptuno
}

void draw() {
  background(0);
  //Creamos el sistema de luces
  lights();
  pointLight(200, 200, 200, 0, 0, 0);
  shape(sol);//render del sol

  for (int i=0; i<sistema.size(); i++) {
    sistema.get(i).render();      //render de los planetas
  }
}
